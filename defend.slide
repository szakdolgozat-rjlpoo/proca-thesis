# Termékkatalógus valós idejű árváltozás követéssel mikroszerviz alapokon

Görög László
laszlogorog93@gmail.com

## Szakdolgozat célja

## Szakdolgozat célja

1. Reaktív programozás alkalmazhatósága Java, Spring környezetben
2. Kapcsolódó technológiák alkalmazása
    - Mikroszerviz architechtúra
    - WebSocket
    - Pub/Sub

## Reaktív programozás

## Reaktív programozás

- Gyökerei az 1970-es évek
- Napjaikban kezd igazán teret nyerni
    - mikroszervizek, több magos processzorok
- Üzleti világban megnövekedett az érdeklődés
    - Nyelvek igyekeznek natív támogat / könyvtárakkal elérhetővé tenni
- Java programozási nyelv esetén 9-es verziótól natív interfész
- 8-as verzióban könyvtárak (RxJava, Reactor, Ratpack, Akka)
- Spring keretrendszer 5-ös verziójától Project Reactor

## Reaktív programozás - Alapok

- deklaratív programozási paradigma - adatfolyamok (statiku, dinamikus), változás propagálás

## Reaktív programozás - Változók kapcsolata

&nbsp;

```
a = b + c
```

- imperatív fejlesztés esetén `a` értékének meghatározása után `b` és `c` változása nem befolyásolja `a` értékét
- reaktív programozás esetén `b` és `c` változása módosítja `a` értékét, anélkül, hogy
explicit újra végrehajtanánk az összeadást

A reaktív programozásnak ezen funkciója felhasználói felületek interakciójánál kiválóan alkalmazható.

## Reaktív programozás - Folyamok

- folyamok célja aszinkron folyamfeldolgozás, nem blokkoló háttérnyomás (back pressure) esetén
    - háttérnyomás - az adat előállító folyamat maximálisan olyan sebességgel termel, amilyen gyorsan a
fogyasztó feltudja azt dolgozni.

- teljesítmény - teljes átviteli sebességet kihasználva

## Reaktív programozás - Példa

**Imperatív - Aszinkron**:

```
 // AsyncRestTemplate
String url = uriBuilder.path("/products").query(builder.build(pageable)).build()
CompletableFuture<ResponseEntity<ProductResponse>> future = this.template.exchange(
    url, HttpMethod.GET, null, new ParameterizedTypeReference<Page<ProductResponse>>() {})
    .completable();

 // Valahol várakozni kell
CompletableFuture.allOf(future, ...).join();
 // Vagy callback, végrehajtás befejezésekor hívódik meg
future.thenApply((responseEntity) => { /* ... */ });
```

**Reaktív**:

```
 // WebClient
Mono<OutputPayload<ProductResponse>> monoPayload = this.webClient.get()
    .uri(uriBuilder -> uriBuilder.path("/products").query(builder.build(pageable)).build())
    .retrieve()
    .bodyToMono(new ParameterizedTypeReference<Page<ProductResponse>>() {});

 // Mono és Flux használata esetén nincs explicit várakozás pont
```

## Alkalmazás

## Alkalmazás - Mikroszerviz architechtúra

- frontend app
    - SPA - JavaScript, Angular
    - átjáró - Java 8, Spring 5, Project Reactor
- tremékek
    - logika - Java 8, Spring 5, Project Reactor
    - adattárolás - PostgreSQL
- készlet
    - logika - Java 8, Spring 5, Project Reactor
    - adattárolás - MongoDB
    - átjáró - Go
- kommunikáció - HTTP, redis

## Alkalmazás - Bemutató

--

## Felmerülő problémák

## Felmerülő problémák - DATACMNS-1133

A hibát 2017-ben jelezték a Spring hibakezelőjében. A hiba az, hogy **reactív repository**
használata esetén, **nincs lehetősége** a Spring által javasolt módon **alapértelmezett adattal
feltölteni egy MongoDB adatbázist**. Ez főként fejlesztés közben jelent problémát,
amikor a könnyebb tesztelhetőség érdekében adat betöltésre lenne szükség.

## Felmerülő problémák - spring-boot#10129

Amennyiben egy Spring alkalmazás **Spring Boot WebFlux komponensre épül** nem
pedig Spring Boot Web, tehát **nem servlet alapú** alkalmazás, **nincs lehetőség kontextus
útvonal megadására**, ami egy alapértelmezett előtag az alkalmazás végpontjai előtt.
Ezen előtag segítségével például verziózhatóvá válnak az útvonalak, azáltal, hogy a
végpontok egyes verzióit más-más modul kezeli.

## Felmerülő problémák - SPR-14335

Teszteléskor gyakran úgynevezett **mock** komponenst használunk, ennek a mocknak a
segítségével megbizonyosodhatunk, hogy az alkalmazás megfelelően használ egy 
interfészt és nincs szükség a teljes környezetet létrehozni. Az SPR-14335 2016-ban
létrehozott hibajegy egy ilyen mock komponens igényét jelzi a Spring Boot fejlesztői
számára, ugyanis a szakdolgozat készítésének ideje alatt még mindig **nincs lehetőség
kicserélni a „ApplicationEventPublisher”** komponenst.

## Tapasztalatok

## Tapasztalatok

- egy metódus vagy módosítsa a beadott paramétereket vagy adjon vissza egy objektumot
- régi paradigma
    - elemek nagy része blokkol
    - aszinkron elemből lehet szinkron elemet létrehozni blokkolás segítségével, de ez
fordítva nem igaz
- WebSocket
    - rengeteg potenciállal rendelkezik
- Mikroszerviz architektúra
    - kisebb egységek sokkal átláthatóbbak és
    - könnyebben kezelhetőek
    - hibák egyértelműen meghatározhatók és kezelhetők
